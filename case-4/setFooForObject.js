/*
  case simples com função com side effects
*/

function setFooForObject(object, value) {
  // TODO: caso `value` for undefined, não alterar o objeto

  object.foo = value

  return object
}

module.exports = setFooForObject
