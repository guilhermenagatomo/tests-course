var expect = require('chai').expect
var setFooForObject = require('../setFooForObject')

var myObject = {}

describe('setFooForObject', () => {
  it('should set the foo property of the passed object', () => {
    setFooForObject(myObject, 'test')

    expect(myObject.foo).to.equal('test')
  })

  it('should not set the property when no value is passed', () => {
    // escreva o teste aqui
  })
})
