var expect = require('chai').expect
var myLib = require('../myLib')

describe('myLib', () => {
  describe('#sum', () => {
    it('should sum two numbers', () => {
      expect(myLib.sum(1, 2)).to.equal(3)
    })
  })

  describe('#subtract', () => {
    it('should subtract two numbers', () => {
      expect(myLib.subtract(2, 1)).to.equal(1)
    })
  })
})
