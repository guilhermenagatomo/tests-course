/*
  case simples de lib com métodos
*/

var myLib = {
  sum(a, b) {
    return a + b
  },

  subtract(a, b) {
    return a - b
  },

  multiply(a, b) {
    return a * b
  }
}

module.exports = myLib
