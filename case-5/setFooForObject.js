/*
  case simples com função com side effects e hooks
*/

function setFooForObject(object, value) {
  if (!object.hasOwnProperty('foo')) {
    object.foo = value
  }

  return object
}

module.exports = setFooForObject
