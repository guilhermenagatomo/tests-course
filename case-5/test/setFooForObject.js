var expect = require('chai').expect
var setFooForObject = require('../setFooForObject')

var myObject = {}

// é executado antes de cada chamada do `it`
beforeEach(() => {
})

describe('setFooForObject', () => {
  it('should set the foo property of the passed object', () => {
    setFooForObject(myObject, 'test')

    expect(myObject.foo).to.equal('test')
  })

  it('should set the foo property of the passed object', () => {
    setFooForObject(myObject, 'test 2')

    expect(myObject.foo).to.equal('test 2')
  })
})
