var expect = require('chai').expect
var myLib = require('../myLib')

describe('myLib', () => {
  describe('#sumAsync', (done) => {
    myLib.sumAsync(1, 1, (result) => {
      expect(result).to.equal(2)
      // quando testes assíncronos são realizados
      // é necessário finalizar o teste com o
      // callback do mocha `done`
      done()
    })
  })

  // escreva um teste para myLib.multiplyAsync
})
