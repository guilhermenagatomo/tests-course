/*
  case de lib com métodos async
*/

var myLib = {
  sumAsync(a, b, callback) {
    setTimeout(() => {
      callback(a + b)
    }, 100)
  },

  multiplyAsync(a, b, callback) {
    setTimeout(() => {
      callback(a * b)
    }, 100)
  }
}

module.exports = myLib
